# iTasks serial

iTasks integration for the serial port libary

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`itasks-serial` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
