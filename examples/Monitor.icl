module Monitor

import StdEnv

import Data.Either
import Data.Func
import System.Time

import iTasks

import iTasks.Extensions.Serial

Start w = flip doTasks w $
	parallel [] [OnAction (Action "+") $ always (Embedded, addDevice)]
		<<@ ArrangeWithTabs True
	>>* [OnAction (Action "Shutdown") $ always (shutDown 0)]
where
	addDevice :: (SharedTaskList ()) -> Task ()
	addDevice stl = tune (Title "New device") $
		           accWorld getTTYDevices
		>>- \ds->  enterChoice [] ["-- Other --":ds] <<@ Title "Choose path"
		>>? \path->updateInformation [] {zero & devicePath=path} <<@ Title "TTY Settings"
		>>! \ts->  appendTask Embedded (\_->monitor ts <<@ Title path) stl
		@! ()

monitor :: TTYSettings -> Task ()
monitor ts = catchAll (
		withShared ([], [], False) \channels->
		    syncSerialChannel
				{tv_sec=0,tv_nsec=100*1000000}          //poll interval (100 ms)
				ts                                      //tty settings
				id                                      //encode
				(\s->(Right (if (s == "") [] [s]), "")) //decode
				channels                                //channels
		||- (viewSharedInformation [ViewAs (take 20 o fst3)] channels <<@ Title "Incoming messages")
		||- forever (
			enterInformation [] <<@ Title "Send line of text"
			>>? \line->upd (\(r,w,s)->(r,w++[line+++"\n"],s)) channels
		) @? const NoValue
	) (\e->viewInformation [] e <<@ Title "Exception occured")
	>>* [OnAction (Action "Close") $ always (treturn ())]
