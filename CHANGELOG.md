# Changelog

#### v0.1.4

- Chore: Upgrade to iTasks 0.16 and base 3

#### v0.1.3

- Chore: Upgrade to iTasks 0.14

#### v0.1.2

- Chore: Upgrade to iTasks 0.13

#### v0.1.1

- Support iTasks 0.7 also

### v0.1

- Initial nitrile version
